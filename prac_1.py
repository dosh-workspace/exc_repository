import math
# make sequence of numbers in place
def made_List():
	baseList=filter(lambda num: num<58 or num>64 ,range(48,91) )
	return list(map(chr, baseList))


# check if number is valid: 1)if char is not valid as num
# 							2) if char is in sequnce but big than base
def check_Func(numAsString,srcBase):
	FIRST_DIGIT='0'
	LAST_DIGIT='9'
	FIRST_CHARACTER='A'
	LAST_CHARACTER='Z'
	digitList=made_List()
	maxDigit=digitList[int(srcBase)-1]
	for char in numAsString:
		if ord(LAST_DIGIT)<ord(char)<ord(FIRST_CHARACTER) or ord(char)>ord(maxDigit):
			return False
	return True

def check_Valid_base(base1,base2):
	return (base1 in made_List() and  base2 in made_List())


# exchange by 2 steps: 1) exchange to base 10
# 					   2)exchange to dest base
def exchange_Func(numAsString,srcBase,sdtBase):
	resultDecim=0
	l=len(numAsString)-1
	i=0
	seqList=made_List()
	for char in numAsString:
		resultDecim+=seqList.index(char)*pow(int(srcBase),l-i)
		i=i+1
	midResult=[]
	print (resultDecim)
	while resultDecim>0:
		midResult.append(resultDecim%int(sdtBase))
		resultDecim=int(resultDecim/int(sdtBase))
	finalResult=list(map(lambda num:str(num) if num<10 else chr(num+55),midResult))
	finalResult.reverse()
	return "".join(finalResult)

def main ():
	numAsString=input('Please type number to exchange\n')
	srcBase=input ('Please type source base\n')
	sdtBase=input ('Please type destination base\n')
	numAsString=numAsString.upper()
	if not check_Valid_base(srcBase,sdtBase):
		print ("bases not valid")
		return
	if check_Func(numAsString,srcBase)==False:
		print ("\nThe number " +numAsString+ " is not a legal number in base " + str(srcBase)+'\n')
		return
	result=exchange_Func(numAsString, srcBase, sdtBase)
	print ("\nResult For (" +numAsString+ "," +str(srcBase)+ ","+str(sdtBase)+"): " +numAsString+ " is a valid number in base " +str(srcBase) + ":its represent in destination  base ("+str(sdtBase)+"): "+result)
	print ('\n')
	return

main()









