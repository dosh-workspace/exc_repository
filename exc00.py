"""Flappy, game inspired by Flappy Bird.
Exercises
1. Keep score.
2. Vary the speed.
3. Vary the size of the balls.
4. Allow the bird to move forward and back.
"""

from random import *
from turtle import *
from freegames import vector
import time

bird = vector(0, 0)
balls = []
size=randint(5,60)  # random balls size each play
velocity=randint(5,20) # random balls velocity each game
start = time.time() # start recording time to compute score


def forw():
    ff=vector(0,30)
    bird.move(ff)

def left():  # go left with array key
    lf=vector(-30,0)

def right(): # go right with array key
    rf= vector(30, 0)
    bird.move(rf)

def inside(point):
    "Return True if point on screen."
    return -200 < point.x < 200 and -200 < point.y < 200

def draw(alive):
    "Draw screen objects."
    clear()
    goto(bird.x, bird.y)
    if alive:
        dot(10, 'green')
    else:
        dot(10, 'red')
        end = time.time()
        print("score is: "+str(int(end - start)*10))

    for ball in balls:
        goto(ball.x, ball.y)
        dot(20, 'black')
    update()

def move():
    "Update object positions."
    bird.y -= 5
    for ball in balls:
      ball.x -= velocity
    if randrange(10) == 0:
        y = randrange(-199, 199)
        ball = vector(199, y)
        balls.append(ball)

    while len(balls) > 0 and not inside(balls[0]):
        balls.pop(0)
    if not inside(bird):
        draw(False)
        return

    for ball in balls:
        if abs(ball - bird) < 15:
            draw(False)
            return
    draw(True)
    ontimer(move, 50)

def game():
    setup(420, 420, 370, 0)
    hideturtle()
    up()
    onkey(left, "Left")
    onkey(right, "Right")
    onkey(forw, "Up")
    tracer(False)
    listen()
    move()
    done()

game() 